﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Inventory : MonoBehaviour {

	public int max_items = 28;

	public GUIElement Inventory_panel;
	
	private List<Item> items_inventory = new List<Item>();
	private Dictionary<string, Item> item_presets = new Dictionary<string, Item>();


	void Start()
	{
	}


	public void setPreset(string name, Item preset)
	{
		item_presets[name] = preset;
	}


	private void fillPresets()
	{
		string[] lines = File.ReadAllLines("./assets/Presets.txt");

		for( int i=0; i < lines.Length; )
		{
			Item preset = new Item(
				lines[i++],
				int.Parse( lines[i++] )
			);

			setPreset(preset.name, preset);
		}
	}


	public Item addItemToInventory(string name)
	{
		Item preset = item_presets[name];	// Get the item
		Item new_item = new Item(preset);	// Create a clone

		items_inventory.Add(new_item);

		return new_item;
	}
}
