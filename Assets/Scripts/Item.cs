﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item {

	public string name;
	public int cost;
	public Sprite image;

	public Item(Item preset)
	{
		name = preset.name;
		cost = preset.cost;
	}

	// Use this for initialization
	public Item(string item_name, int item_cost)
	{
		name = item_name;
		cost = item_cost;
	}
}
