﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public float movement_speed = 5.0f;	// Speed the character walks at
	public float sprint_modifier = 2.0f;
	public float jump_height = 5.0f;	// How high the character jumps
	public GameObject camera;
	
	private Vector3 movement;	// Direction where the character will move to
	private bool sprinting;
	protected Vector3 gravity = Vector3.zero;

	private CharacterController character_controller;


	void Awake()
	{
		// Get component(s)
		character_controller = this.GetComponent<CharacterController>();
	}


	void Update()
	{
		// Set movement based on user input
		movement = new Vector3( Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical") );
		
		// set direction in world space depending on movement speed and if the character is sprinting (relative to the camera)
		if( Input.GetKey(KeyCode.LeftShift) )
			movement = camera.transform.TransformDirection(movement) * movement_speed * sprint_modifier;
		else
			movement = camera.transform.TransformDirection(movement) * movement_speed;
		
		movement.y = 0;

		// If character is in the air
		if( !character_controller.isGrounded )
		{
			gravity += Physics.gravity * Time.deltaTime;
		}
		// If character is on a surface
		else
		{
			gravity = Vector3.zero;

			// Character Jump
			if( Input.GetButton("Jump") )
				gravity.y = jump_height;
		}

		movement += gravity;
		
		character_controller.Move(movement * Time.fixedDeltaTime);	// Apply character movement
	}


	public void toggleVisibility(bool set_visible)
	{
		foreach ( Renderer r in this.GetComponentsInChildren<Renderer>() )
			r.enabled = set_visible;
	}

	public void rotate(Vector3 rotation)
	{
		this.transform.eulerAngles = rotation;
	}
	
}
