﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public GameObject camera_target;	// store target object
	public GameObject player;	// player

	public bool lock_cursor = true;	// If the mouse is locked (centered in screen and invisible)
	public float mouse_sensitivity = 10.0f;	// Sensitivity of looking around
	public Vector2 pitch_min_max = new Vector2(-60, 60);	// How high and low you are allowed to look

	public float thirdperson_zoom_sensitivity = 1.0f; // how fast the camera zooms in and out
	public float thirdperson_zoom = 2.0f;	// How far the camera is zoomed in
	public float thirdperson_zoom_min = 1.0f;	// How far you can zoom out
	public float thirdperson_zoom_max = 6.0f;	// How far you can zoom in
	public float thirdperson_rotation_smooth = 4.0f;
	public float thirdperson_smooth_time = 0.12f;	// how smooth the camera moves
	private bool thirdperson_smooth_enabled = true;

	private float thirdperson_focus_mouse_timer = 0f;	// Timer that starts when thirdperson_focus_mouse is enabled
	public float thirdperson_focus_mouse_time = 5.0f;	// how long the timer should be
	private bool thirdperson_focus_mouse = false;	// If the mouse rotates around the target or if target is focused at mouse
	private bool firstperson = false;	// If camera is in first or third person.

	private new Camera camera;	// script's camera
	private PlayerController player_controller;

	private float yaw;	// y rotation
	private float pitch;	// x rotation

	private Vector3 rotation_smooth_velocity;
	private Vector3 current_rotation;



	void Start()
	{
		camera = this.GetComponent<Camera>();
		player_controller = player.GetComponent<PlayerController>();

		lockCursor();
	}


	void LateUpdate()
	{
		yaw += Input.GetAxis("Mouse X") * mouse_sensitivity;
		pitch -= Input.GetAxis("Mouse Y") * mouse_sensitivity;
		pitch = Mathf.Clamp( pitch, pitch_min_max.x, pitch_min_max.y );


		// Toggle camera mode when Q is pressed
		if( Input.GetKeyDown(KeyCode.Q) )
		{
			firstperson = !firstperson;
			thirdperson_focus_mouse_timer = 0.0f;
		}

		// Toggle mouse locked when tab is pressed
		if( Input.GetKeyDown(KeyCode.Tab) )
		{
			lock_cursor = !lock_cursor;
			lockCursor();
		}

		// If Mouse0 or mouse1 is down; enable mousefocus mode and set timer
		if( Input.GetKey(KeyCode.Mouse0) || Input.GetKey(KeyCode.Mouse1) )
		{
			thirdperson_focus_mouse = true;
			thirdperson_focus_mouse_timer = thirdperson_focus_mouse_time;
		}

		// If timer is finished; set focusmouse mode off
		if( thirdperson_focus_mouse_timer <= 0.0f )
			thirdperson_focus_mouse = false;


		// if in firstperson mode
		if( firstperson )
		{
			thirdperson_smooth_enabled = false;
			player_controller.toggleVisibility(false);
			
			player_controller.rotate( new Vector3(0, yaw, 0) );
			camera.transform.position = camera_target.transform.position; // Place camera at right position (example player's head)
			camera.transform.localEulerAngles = new Vector3(pitch, yaw, 0);	// rotate camera
		}

		// if in thirdperson mode
		else if( !firstperson )
		{
			player_controller.toggleVisibility(true);
			thirdperson_zoom += -Input.GetAxis("Mouse ScrollWheel") * thirdperson_zoom_sensitivity;

			if( thirdperson_zoom < thirdperson_zoom_min )
				thirdperson_zoom = thirdperson_zoom_min;
			if( thirdperson_zoom > thirdperson_zoom_max )
				thirdperson_zoom = thirdperson_zoom_max;


			// If  focus mouse mode is enabled Reduce timer and make the target face at where the camera is looking
			if( thirdperson_focus_mouse )
			{
				player_controller.rotate(new Vector3(player.transform.localEulerAngles.x, camera.transform.localEulerAngles.y, player.transform.localEulerAngles.z));
				thirdperson_focus_mouse_timer -= Time.deltaTime;
			}

			// If the target is moving while focusmouse mode is disabled; make the target face where the target is moving
			if( !thirdperson_focus_mouse & (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) )
			{
				Vector3 look_rotation = camera.transform.TransformDirection( new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) );
				look_rotation.y = 0;
				player.transform.rotation = Quaternion.Slerp(
					player.transform.rotation,
					Quaternion.LookRotation( look_rotation ),
					Time.deltaTime * thirdperson_rotation_smooth
				);
			}

			// rotate and position camera

			if(thirdperson_smooth_enabled)
				current_rotation = Vector3.SmoothDamp( current_rotation, new Vector3(pitch, yaw), ref rotation_smooth_velocity, thirdperson_smooth_time );
			else
			{
				current_rotation = Vector3.SmoothDamp( current_rotation, new Vector3(pitch, yaw), ref rotation_smooth_velocity, 0 );
				thirdperson_smooth_enabled = true;
			}

			camera.transform.eulerAngles = current_rotation;
			camera.transform.position = camera_target.transform.position - transform.forward * thirdperson_zoom;


			// Camera collision (compensate for walls)
			Debug.DrawLine(camera_target.transform.position, camera.transform.position);	// draw line from player to camera

			RaycastHit hit = new RaycastHit();
			LayerMask layer_mask = 1 << 0;
			if( Physics.Linecast(camera_target.transform.position, camera.transform.position, out hit, layer_mask) )
			{
				Debug.DrawRay(hit.point, hit.normal, Color.red);
				camera.transform.position = hit.point;
			}
		}
	}


	void lockCursor()
	{
		if(lock_cursor)
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
		else
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}
	}
}
